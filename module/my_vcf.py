#! /usr/bin/env python3
'''use PyVCF instead'''

import vcf

class MyVCF():
    def __init__(self , vcf_file):
        self.reader = vcf.Reader(filename = vcf_file, compressed = True)
        
    def get_snp_from_a_region(self, chr, start, end):
        #print(chr, start ,end)
        return self.reader.fetch(chr , start, end)
    
    def get_AF_from_a_region(self , record):
        return [ (i.POS , i.INFO['AF'][0]) for i in record ]


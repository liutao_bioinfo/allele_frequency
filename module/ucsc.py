#! /usr/bin/env python3
import unittest


class UCSCGeneTable():
    '''
    this module used to parse format like this :
    #bin    name    chrom   strand  txStart txEnd   cdsStart    cdsEnd  exonCount   exonStarts  exonEnds    score   name2   cdsStartStat    cdsEndStat  exonFrames
    739 NM_000300   chr1    -   20301923    20306932    20302193    20305266    6   20301923,20304507,20304872,20305226,20306072,20306854,  20302336,20304614,20305017,20305372,20306183,20306932,  0   PLA2G2A cmpl    cmpl    1,2,1,0,-1,-1,
    this format can be seen from uscs table broswer or ftp
    '''
    def __init__(self, content):
        tmp = content.rstrip().split()
        self.bin = tmp[0]
        self.name = tmp[1]
        self.chrom = tmp[2]
        self.strand = tmp[3]
        self.tx_start = tmp[4]
        self.tx_end = tmp[5]
        self.cds_start = tmp[6]
        self.cds_end = tmp[7]
        self.exon_count = tmp[8]
        self.exon_starts = eval(tmp[9])
        self.exon_ends = eval(tmp[10])
        self.score = tmp[11]
        self.name2 = tmp[12]
        self.cds_start_stat = tmp[13]
        self.cds_end_stat = tmp[14]
        self.exon_frames = tmp[15]

    def get_exon_list(self):
        return list( zip( self.exon_starts , self.exon_ends ))
        
class TestMyUCSC(unittest.TestCase):
    def test_OldPoolingTable(self):
        content = '922  NM_001078175    chr6    +   44191240    44201888    44195050    44201265    14  44191240,44193797,44194999,44197126,44197325,44197643,44198083,44198304,44198547,44199100,44199734,44200079,44200543,44201153,  44191378,44193904,44195079,44197208,44197528,44197783,44198218,44198402,44198626,44199198,44199843,44200165,44200743,44201888,  0   SLC29A1 cmpl    cmpl    -1,-1,0,2,0,2,1,1,0,1,0,1,0,2,'
        a_gene = UCSCGeneTable(content)
        self.assertListEqual( a_gene.get_exon_list() , [(44191240, 44191378), (44193797, 44193904), (44194999, 44195079), (44197126, 44197208), (44197325, 44197528), (44197643, 44197783), (44198083, 44198218), (44198304, 44198402), (44198547, 44198626), (44199100, 44199198), (44199734, 44199843), (44200079, 44200165), (44200543, 44200743), (44201153, 44201888)]) 
        self.assertEqual(a_gene.chrom , 'chr4')

if __name__ == '__main__':
       unittest.main()
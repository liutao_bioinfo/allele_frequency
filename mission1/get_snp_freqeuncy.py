#! /usr/bin/env python3
import argparse
import sys
import os
import re
bindir = os.path.abspath(os.path.dirname(__file__))
sys.path.append("{0}/../module".format(bindir))
import ucsc
import my_vcf

__author__='Liu Tao'
__mail__= 'taoliu@annoroad.com'

pat1=re.compile('^\s+$')

class MyTranscript(ucsc.UCSCGeneTable):
    pass

class MyGene():
    def __init__(self , transcripts):
        self.name = transcripts[0].name2
        self.exon_list = transcripts[0].get_exon_list()
        self.chrom = transcripts[0].chrom.replace('chr' , '')
        #print(self.chrom, self.exon_list)
        if len(transcripts) > 1 :
            for a_transcript in transcripts[1:]:
                self.exon_list += a_transcript.get_exon_list()
    
    def get_af(self , vcf_object):
        af = []
        for a_exon in self.exon_list:
            record = vcf_object.get_snp_from_a_region(self.chrom, a_exon[0] , a_exon[1])
            af +=vcf_object.get_AF_from_a_region( record )
        self.af = dict(af)
    def __str__(self):
        #print(self.name ) 
        #print(self.af)
        all_af =self.af.values()
        average = 'unknown'
        if len(all_af) > 0: 
            average = sum(all_af)/len(all_af)
        return( "\t".join([self.name , str(average) , " ".join(map( str , all_af))]))

def main():
    parser=argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog='author:\t{0}\nmail:\t{1}'.format(__author__,__mail__))
    parser.add_argument('-v','--vcf',help='input vcf files',dest='vcf',required=True)
    parser.add_argument('-p','--psl',help='input psl file from ucsc',dest='psl',type=open,required=True)
    parser.add_argument('-o','--output',help='output file',dest='output',type=argparse.FileType('w'),required=True)
    #parser.add_argument('-m','--mm',help='output file',dest='mm',action='store_false')
    args=parser.parse_args()

    all_genes = {}
    vcf_object = my_vcf.MyVCF(args.vcf)
    for line in args.psl:
        if line.startswith('#') or re.search(pat1,line):continue
        a_transcript = MyTranscript(line)
        chr = a_transcript.chrom.replace('chr', '')
        if chr.find('hap') > -1  : continue 
        if not chr in all_genes:
            all_genes[chr] = {}
        if not a_transcript.name2 in all_genes[chr]:
            all_genes[chr][a_transcript.name2] = []
        all_genes[chr][a_transcript.name2].append(a_transcript)
    print("#Gene\tAverage\tAF", file=args.output)    
    for a_chr in all_genes:
        for a_name in all_genes[a_chr]:
            a_gene = MyGene( all_genes[a_chr][a_name] )
            a_gene.get_af( vcf_object )
            print(a_gene , file=args.output)
    

if __name__ == '__main__':
    main()
